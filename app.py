# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 13:54:42 2021

@author: artas
"""

import sys

from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QFileDialog, QGraphicsScene, 
    QVBoxLayout, QWidget
)
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QObject, pyqtSignal, QThreadPool, QRunnable, QMetaObject, Q_ARG 

from ui.main_window import Ui_MainWindow
import new_hyperspy_fun

import hyperspy.api as hs

import logging
import os 
import numpy
import qimage2ndarray
from copy import deepcopy 

from sklearn import preprocessing
from skimage import filters

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT 
from matplotlib.figure import Figure



        
def check_filenames(filenames, labels):
    fnames=[]
    for fname in filenames:
        for label in labels:
            if label in fname:
                fnames.append(fname)
    return(fnames)

def return_all_pathes(directory):
    filenames=[]
    for path, subdirs, files in os.walk(directory):
        for name in files:
            filenames.append(os.path.join(path, name))
    filenames.sort()
    return filenames

def normalize_data(data, norm_parameter="max"):
    if norm_parameter=="max":
        norm_parameter=int(numpy.max(data.data))
    if len(data.data.shape)==3:      
        newdata=numpy.zeros((len(data.data),len(data.data[0]),len(data.data[0][0])))
        for index in range((len(data.data))):
            for index2 in range((len(data.data[0]))):        
                newdata[index][index2]=preprocessing.minmax_scale(data.data[index][index2], feature_range=(0, norm_parameter))
    elif len(data.data.shape)==2:
        newdata=numpy.zeros((len(data.data),len(data.data[0])))
        for index in range((len(data.data))):       
            newdata[index]=preprocessing.minmax_scale(data.data[index], feature_range=(0, norm_parameter)) 
    elif len(data.data.shape)==1:
        newdata=preprocessing.minmax_scale(data.data, feature_range=(0, norm_parameter))
    else:
        raise TypeError('The spectrum array size is not supported. Should be 1, 2 or 3 dimensional')      
    return newdata
        
def image_3D_to_2D(image):
    image=numpy.sum(image, axis=2)
    return image
    
        
def image_remove_nan_and_outliers(image, sigma_interval=5):
    image=image_3D_to_2D(image)
    image=numpy.nan_to_num(image)

    there_was_zero=False
    for index in range(len(image)):
        for index2 in range(len(image[0])):
            if image[index][index2]==0 or image[index][index2]<0:

                points=0
                value=0
                if index+1 < len(image) and image[index+1][index2]>0:
                    value+=image[index+1][index2]
                    points+=1
                if index-1 > 0 and image[index-1][index2]>0:
                    value+=image[index-1][index2]
                    points+=1
                if index2+1 < len(image[index]) and image[index][index2+1]>0:
                    value+=image[index][index2+1]
                    points+=1
                if index2-1 > 0 and image[index][index2-1]>0:
                    value+=image[index][index2-1]
                    points+=1
                if points==0:
                    points=1
                    there_was_zero=True
                    
                image[index][index2]=value/points
                
    if there_was_zero==True:
        for index in range(len(image)):
            for index2 in range(len(image[0])):
                if image[index][index2]==0 or image[index][index2]<0:
    
                    points=0
                    value=0
                    if index+1 < len(image) and image[index+1][index2]>0:
                        value+=image[index+1][index2]
                        points+=1
                    if index-1 > 0 and image[index-1][index2]>0:
                        value+=image[index-1][index2]
                        points+=1
                    if index2+1 < len(image[index]) and image[index][index2+1]>0:
                        value+=image[index][index2+1]
                        points+=1
                    if index2-1 > 0 and image[index][index2-1]>0:
                        value+=image[index][index2-1]
                        points+=1
                    if points==0:
                        points=1
                        
                    image[index][index2]=value/points
        
    # calculate summary statistics
    data_mean, data_std = numpy.mean(image), numpy.std(image)
    # identify outliers
    cut_off = data_std * sigma_interval
    lower, upper = data_mean - cut_off, data_mean + cut_off
    logging.debug("Cutoff values for the points in the generated image:" + str(lower)+","+ str(upper))
    number=0
    if sigma_interval!=0:
        for index in range(len(image)):
            for index2 in range(len(image[0])):
                if image[index][index2]<lower or image[index][index2]>upper:
                    number+=1
                    points=0
                    value=0
                    if index+1 < len(image) and image[index+1][index2]>lower and image[index+1][index2]<upper:
                        value+=image[index+1][index2]
                        points+=1
                    if index-1 > 0 and image[index-1][index2]>lower and image[index-1][index2]<upper:
                        value+=image[index-1][index2]
                        points+=1
                    if index2+1 < len(image[index]) and image[index][index2+1]>lower and image[index][index2+1]<upper:
                        value+=image[index][index2+1]
                        points+=1
                    if index2-1 > 0 and image[index][index2-1]>lower and image[index][index2-1]<upper:
                        value+=image[index][index2-1]
                        points+=1
                    image[index][index2]=value/points
    logging.debug(str(number)+" points replaced")
    return image

def integrate_spectrum(losses, mask):
    if len(mask) != 0:
        losses[mask]=float('nan')
   
    reshaped=losses.reshape((len(losses)*len(losses[0]),len(losses[0][0])))
    nanremoved=reshaped[~numpy.isnan(reshaped).any(axis=1)]
    spectrum=numpy.sum(nanremoved, axis=0)/len(nanremoved)
    return spectrum

def create_spectra(spectra, plot_indeces, gaus_filter=0):
    spectra_out=[]
    for plot_index in plot_indeces:
        
        energies=numpy.arange(spectra[plot_index].axes_manager["Energy loss"].offset,
                              spectra[plot_index].axes_manager["Energy loss"].offset+
                              (spectra[plot_index].axes_manager["Energy loss"].size)*
                              spectra[plot_index].axes_manager["Energy loss"].scale,
                              spectra[plot_index].axes_manager["Energy loss"].scale)
 
        losses=deepcopy(spectra[plot_index].data)
        
        if len(spectra[plot_index].data.shape)==3:
            spectrum=integrate_spectrum(losses, [])
        elif len(spectra[plot_index].data.shape)==1:
            spectrum=losses
        else:
            raise ValueError("Not supported data array shape")
        if gaus_filter>0:
            spectrum=filters.gaussian(spectrum, sigma=gaus_filter)
 
        spectra_out.append([energies, spectrum])
    return spectra_out

def create_zl_from_reflected_tail(zl):
    
    energy=zl.axes_manager["Energy loss"].offset
    zero_at=0
    while energy<0:
        zero_at+=1
        energy+=zl.axes_manager["Energy loss"].scale
    if len(zl.data.shape)==3:  
        newdata=numpy.zeros((len(zl.data),len(zl.data[0]),len(zl.data[0][0])))
        for index in range(len(zl.data)):
            for index2 in range(len(zl.data[index])):    
                #zero_at=numpy.where(zl.data[index][index2]==numpy.max(zl.data[index][index2]))[0][0]
                if abs(zl.axes_manager["Energy loss"].offset)<=(zl.axes_manager["Energy loss"].offset+zl.axes_manager["Energy loss"].size*zl.axes_manager["Energy loss"].scale):
                    newd=numpy.concatenate((zl.data[index][index2][0:zero_at], zl.data[index][index2][0:(zero_at-1)][::-1]))
                    newdata[index][index2]=numpy.concatenate((newd, numpy.zeros((len(zl.data[0][0])-2*zero_at+1))))
                else:
                    newdata[index][index2]=numpy.concatenate((zl.data[index][index2][0:zero_at], zl.data[index][index2][0:(zero_at-1)][::-1]))[0:zl.axes_manager["Energy loss"].size]
                    
    elif len(zl.data.shape)==2: 
        newdata=numpy.zeros((len(zl.data),len(zl.data[0])))
        for index in range(len(zl.data)):   
            #zero_at=numpy.where(zl.data[index][index2]==numpy.max(zl.data[index][index2]))[0][0]
            newd=numpy.concatenate((zl.data[index][0:zero_at], zl.data[index][0:(zero_at-1)][::-1]))
            newdata[index]=numpy.concatenate((newd, numpy.zeros((len(zl.data[0])-2*zero_at+1))))
    else:
        raise ValueError('The spectrum array size is not supported. Should be 2 or 3 dimensional')
    return newdata

def fit_to_gaus_and_lorentz(signal, range_energy):
    signal=signal.sum() 
    
    m2 = signal.create_model()
    g1 = hs.model.components1D.Gaussian()
    l1 = hs.model.components1D.Lorentzian()
    m2.extend((g1, l1))
    m2['PowerLaw'].active = False
    m2.set_signal_range(range_energy[0], range_energy[1])
    m2.fit()
    m2.print_current_values()
    """
    #recentering, if it is needed
    min_energy=m2.axes_manager["Energy loss"].offset
    max_energy=m2.axes_manager["Energy loss"].offset+m2.axes_manager["Energy loss"].size*m2.axes_manager["Energy loss"].scale
    new_offset=(abs(min_energy)+abs(max_energy))/2
    m2.axes_manager["Energy loss"].offset=-new_offset
    #
    """
    return m2.as_signal()

def save_spectra(folder, spectra):
    for spectrum in spectra:
        if os.path.isfile(folder+"/"+ spectrum[1]+r".rpl")==0:
            spectrum[0].save(folder+"/"+ spectrum[1]+r".rpl")
        else:
            logging.info("File(s) with the same name(s) already exist, no data was saved")


class QTextEditLogger(logging.Handler):
    def __init__(self, parent, widget):
        super().__init__()
        self.widget=widget
        self.widget.setReadOnly(True)

    def emit(self, record):
        QMetaObject.invokeMethod(self.widget, 
                "append",  
                Qt.QueuedConnection,
                Q_ARG(str, self.format(record)))
        

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class plotter_window(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(plotter_window, self).__init__(*args, **kwargs)

        # Create the maptlotlib FigureCanvas object, 
        # which defines a single set of axes as self.axes.
        self.sc = MplCanvas(self, width=5, height=4, dpi=100)
        
        # Create toolbar, passing canvas as first parament, parent (self, the MainWindow) as second.
        toolbar = NavigationToolbar2QT(self.sc, self)

        layout = QVBoxLayout()
        layout.addWidget(toolbar)
        layout.addWidget(self.sc)

        # Create a placeholder widget to hold our toolbar and canvas.
        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        
    
    def plot(self, spectra):
        self.sc.axes.cla()
        for spectrum in spectra:
            self.sc.axes.plot(spectrum[0], spectrum[1])
        self.sc.draw() 

class Signals(QObject):
    finished = pyqtSignal(object)
      
class data_loader(QRunnable):
    def __init__(self, fnames, gen_images=False, pattern=""):
        super().__init__()
        self.signal = Signals()
        
        self.fnames=fnames
        self.gen_images=gen_images
        self.pattern=pattern
        

    def run(self):
        #self.progress.emit(i + 1)
        self.data=[]
        self.data_zlcentered=[]
        self.images_2D=[]
        self.new_fname=[]
        #num_of_files=len(self.fnames)
        #logging.info(str(num_of_files)+" file(s) to be loaded")
        for fname in self.fnames:
            data_temp=hs.load(fname)
            if data_temp.metadata.Signal.signal_type==self.pattern:
                self.new_fname.append(fname)
                self.data.append(data_temp)
                if self.gen_images==True:
                
                    self.images_2D.append(image_remove_nan_and_outliers(data_temp.data))
                
                data_zlcentered_temp=deepcopy(data_temp)
                data_zlcentered_temp.align_zero_loss_peak()
                data_zlcentered_temp.data=normalize_data(data_zlcentered_temp)
                self.data_zlcentered.append(data_zlcentered_temp)  
                logging.info(fname+" loaded, centered and normalized")
            else:
                logging.info(fname+" is not "+str(self.pattern)+" type")
            #num_of_files-=1
            #logging.info(str(num_of_files)+" file(s) left")
                
        self.signal.finished.emit([self.data, self.data_zlcentered, self.images_2D, self.new_fname])  

class eels_deconvolver(QRunnable):
    def __init__(self, data_eels, filenames, zl_method, thresholds=[0.01], calculate_no_cov=True, 
                 max_iterations=1000, zero_losses=None, zl_fnames=None,
                 rebin=False, smoothen_zl=0):
        super().__init__()
        self.signal = Signals()
        
        self.data_eels=data_eels
        self.filenames=filenames
        self.zl_method=zl_method
        self.thresholds=thresholds
        self.calculate_no_cov=calculate_no_cov 
        self.max_iterations=max_iterations
        self.zero_losses=zero_losses
        self.zl_fnames=zl_fnames
        self.rebin=rebin
        self.smoothen_zl=smoothen_zl

    def run(self):  
        results=[]
        num_files=len(self.data_eels)
        logging.info("Deconvolution initialized, file(s) left: "+str(num_files))
        
        for index_d in range(num_files):
            data=self.data_eels[index_d] 
            data=deepcopy(data)
            fname=self.filenames[index_d]
            folder_name=os.path.basename(os.path.dirname(fname))
            
            if self.rebin==True:
                data=data.rebin(scale=[2, 2, 1])
            
            if self.zl_method=="Refl. tail":
                zl=deepcopy(data)
                zl.data=create_zl_from_reflected_tail(zl)
                logging.info("ZL from reflected tail generated")
                zl=fit_to_gaus_and_lorentz(zl, [zl.axes_manager["Energy loss"].offset, -zl.axes_manager["Energy loss"].offset])
                logging.info("ZL fitted by Gaussian and Lorentzian")
                zl.data=normalize_data(zl, max(data.data[0][0]))
                

                for threshold in self.thresholds:
                    deconv=data.richardson_lucy_deconvolution_with_threshold(zl, iterations=self.max_iterations, threshold=threshold, parallel=True)
                    logging.info(fname+" deconvolution performed")
                    logging.info(deconv.metadata.General.title)
                    results.append([deconv, folder_name+"_deconvolution_th = "+str(threshold)+"_"+self.zl_method])
            elif self.zl_method=="ZL data":
                
                for zl_index in range(len(self.zero_losses)):
                    zl=self.zero_losses[zl_index]
                    zl=deepcopy(zl)
                    zl_fname=self.zl_fnames[zl_index]
    
                    new_min=max(data.axes_manager["Energy loss"].offset, zl.axes_manager["Energy loss"].offset)
                    energies_zl=numpy.arange(zl.axes_manager["Energy loss"].offset,zl.axes_manager["Energy loss"].offset+
                                             zl.axes_manager["Energy loss"].scale*zl.axes_manager["Energy loss"].size, zl.axes_manager["Energy loss"].scale)
        
                    energies_data=numpy.arange(data.axes_manager["Energy loss"].offset,data.axes_manager["Energy loss"].offset+
                                               data.axes_manager["Energy loss"].scale*data.axes_manager["Energy loss"].size, data.axes_manager["Energy loss"].scale)
                    start_index_zl=numpy.argmax(energies_zl>new_min)-1
                    start_index_data=numpy.argmax(energies_data>new_min)-1
                    length=min(data.axes_manager["Energy loss"].size, zl.axes_manager["Energy loss"].size)
                    if (length-start_index_zl)!=(length-start_index_data):
                        length=length-max(start_index_zl, start_index_data)
                        
                    if len(zl.data.shape)==1:
                        zl_method_out="_"+self.zl_method+"_point"
                        zl.data=zl.data[start_index_zl:(length+start_index_zl)]
                        
                    elif len(zl.data.shape)==3:
                        zl_method_out="_"+self.zl_method+"_map"
                        zl.data=zl.data[:,:,start_index_zl:(length+start_index_zl)]
                        zl=zl.sum()                
                    else:
                        raise ValueError("Not supported zero loss array shape")
                    
                    if len(data.data.shape)==3:
                        data.data=data.data[:,:,start_index_data:(length+start_index_data)]
                    else:
                        raise ValueError("Not supported data array shape")
        
                    zl.axes_manager["Energy loss"].size=length
                    zl.axes_manager["Energy loss"].offset=energies_zl[start_index_zl]
                    data.axes_manager["Energy loss"].size=length
                    data.axes_manager["Energy loss"].offset=energies_data[start_index_data]
        
                    
                    zl.data=normalize_data(zl, max(data.data[0][0]))
                    if self.smoothen_zl != 0:
                        zl.data=filters.gaussian(zl.data, sigma=self.smoothen_zl)
                        logging.info("Gaus filter appiled to ZL")
                    
                    for threshold in self.thresholds:
                        deconv=data.richardson_lucy_deconvolution_with_threshold(zl, iterations=self.max_iterations, threshold=threshold, parallel=True)
                        logging.info(fname+" deconvolution performed with "+zl_fname)
                        logging.info(deconv.metadata.General.title)
                        results.append([deconv, folder_name+"_deconvolution_th = "+str(threshold)+"_zlname_"+os.path.basename(os.path.dirname(zl_fname))+zl_method_out])
    
            num_files-=1
            logging.info(str(num_files)+" files left")
            if self.calculate_no_cov==True:
                results.append([data, folder_name+"_no deconvolution"])
                logging.debug("No deconvolution data appended")
            
        self.signal.finished.emit([results, zl])
    

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.connectSignalsSlots()
        
        logTextBox=QTextEditLogger(self, self.textEdit_log)
        #logTextBox.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        logTextBox.setFormatter(logging.Formatter('%(asctime)s - %(message)s',datefmt='%Y-%m-%d,%H:%M:%S'))
        logging.getLogger().addHandler(logTextBox)
        logging.getLogger().setLevel(logging.INFO)
        
        self.scene_map = QGraphicsScene(self)
        self.scene_deconv_map = QGraphicsScene(self)
        
        self.plotter_eels = plotter_window(self)
        self.plotter_eels.setWindowTitle("Spectum for EELS signal")
        self.plotter_zl = plotter_window(self)
        self.plotter_zl.setWindowTitle("Spectum for ZL signal")
        self.plotter_deconv = plotter_window(self)
        self.plotter_deconv.setWindowTitle("Spectum for deconvoluted EELS signal")
        


    def connectSignalsSlots(self):
        self.open_map_file.clicked.connect(lambda: self.get_filename([0, "EELS"]))
        self.open_zlp_file.clicked.connect(lambda: self.get_filename([1, "EELS"]))
        self.open_map_folder.clicked.connect(lambda: self.get_foldername([0, "EELS"]))
        self.open_zlp_folder.clicked.connect(lambda: self.get_foldername([1, "EELS"]))
        
        self.pushButton_whole.clicked.connect(lambda: self.plot_spectrum("whole", self.plotter_eels, 
                                                                               self.center_zlp_data.checkState(), self.data_maps, 
                                                                               self.data_maps_zlc, self.map_slider.value()))
        self.pushButton_zlplot.clicked.connect(lambda: self.plot_spectrum("whole", self.plotter_zl, 
                                                                                self.center_zlp_zlp.checkState(), self.data_zl, 
                                                                                self.data_zl_zlc, self.spinBox_zlnumber.value(), gaus_value=self.zlp_gaus_value.value()))         
        self.plot_deconv.clicked.connect(lambda: self.plot_spectrum("whole", self.plotter_deconv, 
                                                                          0, [row[0] for row in self.deconv_results], 
                                                                          None, self.deconv_map_slider.value()))

        
        self.deconvolute_all.clicked.connect(lambda: self.deconvolute_it("all"))
        self.deconvolute_selected.clicked.connect(lambda: self.deconvolute_it("single"))
        
        
        self.map_slider.valueChanged.connect(self.map_sliderMoved)
        self.deconv_map_slider.valueChanged.connect(self.deconv_map_sliderMoved)
        self.spinBox_zlnumber.valueChanged.connect(self.zl_spinboxchanged)
        
        self.save_deconv.clicked.connect(self.save_deconv_results)
        
    def deconvolute_it(self, deconv_selection):
        if self.zl_method.currentText()=="Refl. tail":
            self.zero_losses=None
            self.zl_fnames=None       
        elif self.zl_method.currentText()=="ZL data":
            self.zero_losses=self.data_zl_zlc
            self.zl_fnames=self.zlname 
            
        self.run_threadTask("eels_deconvolver_"+deconv_selection)
        
    def plot_spectrum(self, region, plotter, center_zlp, data, data_zlc, slider_index, gaus_value=0):
        plotter.show()
        if center_zlp==0:
            spectrum_data=create_spectra(data, [slider_index], gaus_filter=gaus_value)
        else:
            spectrum_data=create_spectra(data_zlc, [slider_index], gaus_filter=gaus_value)
        plotter.plot(spectrum_data)

    def get_filename(self, pattern):
        fname = QFileDialog.getOpenFileNames(self, 'Open file(s)', 'c:\\', "EELS maps/ZL (*.dm3 *.dm4)")
        logging.debug("Selected file name(s):")
        fname=fname[0]
        fname=check_filenames(fname, [".dm3", ".dm4"])
        logging.debug(fname)
        self.load_fname(fname, pattern)  
     
    def get_foldername(self, pattern):
        folderpath = QFileDialog.getExistingDirectory(self, 'Select folder')
        logging.debug("Folder name:")
        logging.debug(folderpath)
        fname=return_all_pathes(folderpath)
        fname=check_filenames(fname, [".dm3", ".dm4"])
        self.load_fname(fname, pattern)
        
    def save_deconv_results(self):
        folderpath = QFileDialog.getExistingDirectory(self, 'Select folder')
        logging.debug("Folder name to save deconv. results:")
        logging.debug(folderpath)
        if len(folderpath)!=0:
            save_spectra(folderpath, self.deconv_results)
        
        
    def run_threadTask(self, threadtype, parameters=None):
        threadCount = QThreadPool.globalInstance().maxThreadCount()
        pool = QThreadPool.globalInstance()
        
        if threadtype=="eels_loader":
            self.open_map_file.setEnabled(False)
            self.open_map_folder.setEnabled(False)
            thread = data_loader(self.fname, gen_images=True, pattern=parameters)
            thread.signal.finished.connect(self.eels_data_loaded)
        elif threadtype=="zl_loader":
            self.open_zlp_file.setEnabled(False)
            self.open_zlp_folder.setEnabled(False)
            thread = data_loader(self.zlname, gen_images=False, pattern=parameters)
            thread.signal.finished.connect(self.zl_data_loaded)
        elif "eels_deconvolver" in threadtype:
            self.deconvolute_selected.setEnabled(False)
            self.deconvolute_all.setEnabled(False)
            if "all" in threadtype:
                thread = eels_deconvolver(self.data_maps_zlc, self.fname, self.zl_method.currentText(), thresholds=[self.deconv_threshold.value()],
                                          calculate_no_cov=True, max_iterations=self.deconv_max_it.value(), zero_losses=self.zero_losses, 
                                          zl_fnames=self.zl_fnames,rebin=self.deconv_data_binning.checkState(), smoothen_zl=self.zlp_gaus_value.value())
            elif "single" in threadtype:
                if self.zl_method.currentText()=="ZL data":
                    self.zero_losses=[self.data_zl_zlc[self.spinBox_zlnumber.value()]]
                    self.zl_fnames=[self.zlname[self.spinBox_zlnumber.value()]] 
                thread = eels_deconvolver([self.data_maps_zlc[self.map_slider.value()]], [self.fname[self.map_slider.value()]],
                                          self.zl_method.currentText(), thresholds=[self.deconv_threshold.value()],
                                          calculate_no_cov=True, max_iterations=self.deconv_max_it.value(), zero_losses=self.zero_losses, 
                                          zl_fnames=self.zl_fnames,rebin=self.deconv_data_binning.checkState(), smoothen_zl=self.zlp_gaus_value.value())
            thread.signal.finished.connect(self.deconv_generated)
            
        pool.start(thread)
        
    def deconv_generated(self, d):
        logging.info("Deconvolution finished")
        self.deconv_results=d[0]
        
        self.deconvolute_selected.setEnabled(True)
        self.deconvolute_all.setEnabled(True)
        
        self.plot_deconv.setEnabled(True)
        self.save_deconv.setEnabled(True)
        
        self.deconv_map_slider.setEnabled(True)
        self.deconv_map_slider.setMinimum(0)
        self.deconv_map_slider.setMaximum(len(self.deconv_results)-1)
        self.deconv_map_slider.setValue(0)
        
        self.deconv_map_sliderMoved(0)
    
    def load_fname(self, fname, pattern):
        if len(fname)!=0: 
            if pattern[0]==0:
                self.fname=fname
                #self.fname=check_filenames(fname, [pattern[1]])
                if len(self.fname)!=0: 
                    logging.info("Potential file name(s) with EELS maps:")
                    logging.info(self.fname)
                    self.run_threadTask("eels_loader", pattern[1])
            else:           
                self.zlname=fname
                if len(self.zlname)!=0: 
                    logging.info("Potential file name(s) with ZL:")
                    logging.info(self.zlname)
                    self.run_threadTask("zl_loader", pattern[1])
                
    def eels_data_loaded(self, d):
        self.open_map_file.setEnabled(True)
        self.open_map_folder.setEnabled(True)
        self.fname=d[3]
        if len(d[0])!=0:
            self.data_maps = d[0]
            self.data_maps_zlc = d[1]
            self.images_maps = d[2]
            
            logging.info("EELS data loaded")
            self.map_sliderMoved(0)
            
            self.map_slider.setEnabled(True)
            self.map_slider.setMinimum(0)
            self.map_slider.setMaximum(len(self.data_maps)-1)
            self.map_slider.setValue(0)
            
            self.pushButton_whole.setEnabled(True)
            
            
            self.zl_method.addItem("Refl. tail")
            self.deconvolute_all.setEnabled(True)
            self.deconvolute_selected.setEnabled(True)
        else:
            logging.info("No EELS data detected")
            
        
    def zl_data_loaded(self, d):
        self.open_zlp_file.setEnabled(True)
        self.open_zlp_folder.setEnabled(True)
        self.zlname=d[3]
        if len(d[0])!=0:
            self.data_zl = d[0]
            self.data_zl_zlc = d[1]
            
            logging.info("ZL data loaded")
            
            self.spinBox_zlnumber.setEnabled(True)
            self.spinBox_zlnumber.setMaximum(len(self.data_zl)-1)
            self.spinBox_zlnumber.setValue(0)
            
            self.pushButton_zlplot.setEnabled(True)
            self.zl_name.setText(self.zlname[0])
            self.zl_method.addItem("ZL data")
        else:
            logging.info("No ZL data detected")
        
        
     
    def show_image_map(self, index=0):
        self.scene_map.clear()
        image=self.images_maps[index]
        image *= 255.0/image.max()
        image=image.astype(int)
        qimage = qimage2ndarray.array2qimage(image)
        pixmap = QPixmap.fromImage(qimage)
        
        pixmap_item=self.scene_map.addPixmap(pixmap)
        
        self.view_map.setScene(self.scene_map)
        self.view_map.fitInView(pixmap_item, Qt.KeepAspectRatio) 
        
    def show_image_deconv_map(self, index=0):
        self.scene_deconv_map.clear()
        image=image_3D_to_2D(self.deconv_results[index][0].data)
        
        image *= 255.0/image.max()
        image=image.astype(int)
        qimage = qimage2ndarray.array2qimage(image)
        pixmap = QPixmap.fromImage(qimage)
        
        pixmap_item=self.scene_deconv_map.addPixmap(pixmap)
        
        self.view_deconv_map.setScene(self.scene_deconv_map)
        self.view_deconv_map.fitInView(pixmap_item, Qt.KeepAspectRatio) 
        
    def map_sliderMoved(self, val):
        logging.debug("EELS slider changed to:"+str(val))
        self.show_image_map(val)
        self.map_name.setText(self.fname[val])
        
    def deconv_map_sliderMoved(self, val):
        logging.debug("Deconv. slider changed to:"+str(val))
        self.show_image_deconv_map(val)
        self.deconv_map_name.setText(self.deconv_results[val][1])
        
    def zl_spinboxchanged(self, val):
        logging.debug("ZL spinbox changed to:"+str(val))
        self.zl_name.setText(self.zlname[val])
        

        

        
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec())