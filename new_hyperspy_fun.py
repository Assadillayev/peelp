# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 11:38:19 2021

@author: artas
"""
from hyperspy._signals.eels import EELSSpectrum_mixin

import logging

from hyperspy.defaults_parser import preferences
import numpy as np
from copy import deepcopy 

def richardson_lucy_deconvolution_with_threshold(self, psf, threshold=None, iterations=15, mask=None,
                                  show_progressbar=None,
                                  parallel=None):
    
    # Modified version of r-l deconvolution impemneted in hyperspy. The main difference is threshold parameter.
    """1D Richardson-Lucy Poissonian deconvolution of
    the spectrum by the given kernel.

    Parameters
    ----------
    iterations: int
        Number of iterations of the deconvolution. Note that
        increasing the value will increase the noise amplification.
    psf: EELSSpectrum
        It must have the same signal dimension as the current
        spectrum and a spatial dimension of 0 or the same as the
        current spectrum.
    show_progressbar : None or bool
        If True, display a progress bar. If None the default is set in
        `preferences`.
    parallel : {None,bool,int}
        if True, the deconvolution will be performed in a threaded (parallel)
        manner.

    Notes:
    -----
    For details on the algorithm see Gloter, A., A. Douiri,
    M. Tence, and C. Colliex. “Improving Energy Resolution of
    EELS Spectra: An Alternative to the Monochromator Solution.”
    Ultramicroscopy 96, no. 3–4 (September 2003): 385–400.

    """
    if show_progressbar is None:
        show_progressbar = preferences.General.show_progressbar
    self._check_signal_dimension_equals_one()
    psf_size = psf.axes_manager.signal_axes[0].size
    kernel = psf()
    imax = kernel.argmax()
    maxval = self.axes_manager.navigation_size
    show_progressbar = show_progressbar and (maxval > 0)

    def deconv_function(signal, threshold, kernel=None,
                        iterations=15, psf_size=None):
        imax = kernel.argmax()
        result = np.array(signal).copy()
        mimax = psf_size - 1 - imax
        for index in range(iterations):
            first = np.convolve(kernel, result)[imax: imax + psf_size]
            result_previous=deepcopy(result)
            result *= np.convolve(kernel[::-1], signal /
                                  first)[mimax:mimax + psf_size]
            if threshold!=None:
                if np.sum(abs(result_previous-result))/np.sum(abs(result))<threshold:
                    result=deepcopy(result_previous)
                    break
        return [result, index]
    
    ds = self.map(deconv_function, kernel=psf, threshold=threshold, iterations=iterations,
                  psf_size=psf_size, show_progressbar=show_progressbar,
                  parallel=parallel, ragged=False, inplace=False)

    iterations=np.average(np.array([item[:,1] for item in ds.data]).astype(float))
    #logging.info(str(iterations)+" iteration(s) performed on average")
    ds.data=np.array([list(item[:,0]) for item in ds.data])
    ds.axes_manager["Energy loss"].size=len(ds.data[0][0])
    ds.metadata.General.title += (
        ' after Richardson-Lucy deconvolution %i iterations' %
        iterations)
    if ds.tmp_parameters.has_item('filename'):
        ds.tmp_parameters.filename += (
            '_after_R-L_deconvolution_%iiter' % iterations)
    return ds


EELSSpectrum_mixin.richardson_lucy_deconvolution_with_threshold = richardson_lucy_deconvolution_with_threshold