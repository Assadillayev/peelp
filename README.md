Installation guide:
1. Download and install anaconda: https://www.anaconda.com/products/individual
2. Open Anaconda Prompt
3. Install required packages:

    conda install -c anaconda pyqt

    conda install hyperspy -c conda-forge

    conda install -c conda-forge qimage2ndarray
    
4. Copy git repository: 

    git clone https://gitlab.com/Assadillayev/peelp.git

5. Run the script: 

    cd peelp
    
    python app.py

